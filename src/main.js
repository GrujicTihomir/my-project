import Vue from 'vue'
import bootstrap from 'bootstrap-vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueRouter from 'vue-router'
import aboutUs from './components/aboutUs.vue'
import home from './components/home.vue'
import contact from './components/contact.vue'
import cart from './components/cart.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faTruck, faShoppingCart, faAngleDoubleLeft, faAngleDoubleRight, faHome, faInfo, faEnvelope, faShoePrints, faFire, faPlus, faMinus, faBars, } from '@fortawesome/free-solid-svg-icons'


Vue.component('font-awesome-icon', FontAwesomeIcon)
library.add( faTruck, faShoppingCart, faAngleDoubleLeft, faAngleDoubleRight, faHome, faInfo, faEnvelope, faShoePrints, faFire, faPlus, faMinus, faBars, )


Vue.use(bootstrap);
Vue.use(VueRouter);

const routes = [
  { path:'/', component: home },
  { path:'/o-nama', component: aboutUs },
  { path:'/kontakt', component: contact },
  { path:'/korpa/:korpa_item', name: 'korpa-item', component: cart },
];


var router = new VueRouter({
  routes,
  mode: 'history',
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
